## Help Message Strings

description = Pakethanterare för kommandoraden utformad för hantering av modifikationer till spel
merge-help = Installera eller ta bort paket
package-meta = PAKET

## Query messages


## Package phase messages


## Module messages


## Dependency messages


# TODO: There are a number of context strings that may eventually be passed to DepError
# which should be internationalized


## Download messages


## Config messages


## News messages


## Flags messages


## Use flag messages


## Conflicts UI Messages


## Select messages


## Profile messages


## Use flag configuration messages


## VFS messages


## Loader messages


## Use string messages


## Questions


## Argparse generic


## Pybuild Messages


## Mirror Messages


## Repo Messages


## Init Messages


## Prefix messages
